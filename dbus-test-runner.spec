%define soname  libdbustest
%define sover   1
%define _version 15.04.0+16.10.20160906
Name:           dbus-test-runner
Version:        15.04.0+bzr20151002
Release:        0
Summary:        Runs tests under a new D-Bus session
License:        LGPL-2.1-or-later
Group:          Development/Tools/Other
URL:            https://launchpad.net/dbus-test-runner
Source:         https://launchpad.net/ubuntu/+archive/primary/+files/%{name}_%{_version}.orig.tar.gz
BuildRequires:  gnome-common, intltool
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-introspection-1.0)

%description
A simple little executable for running a couple of programs under a
new D-Bus session.

%package -n %{soname}%{sover}
Summary:        Runs tests under a new D-Bus session -- Libraries
Group:          System/Libraries
Provides:       %{name} = %{version}

%description -n %{soname}%{sover}
A simple little executable for running a couple of programs under a
new D-Bus session.

This package contains shared libraries.

%package devel
Summary:        Runs tests under a new DBus session -- Development files
Group:          Development/Libraries/C and C++
Requires:       %{soname}%{sover} = %{version}

%description devel
A simple little executable for running a couple of programs under a
new D-Bus session.
This package contains files that are needed to build.

%prep
%setup -q -c

%build
NOCONFIGURE=1 gnome-autogen.sh
%configure --disable-static
make %{?_smp_mflags} V=1

%install
%make_install
find %{buildroot} -type f -name "*.la" -delete -print

%post -n %{soname}%{sover} -p /sbin/ldconfig

%postun -n %{soname}%{sover} -p /sbin/ldconfig

%files
%{_bindir}/%{name}
%{_datadir}/%{name}/
%{_libexecdir}/%{name}/

%files -n %{soname}%{sover}
%{_libdir}/%{soname}.so.%{sover}*

%files devel
%{_includedir}/%{soname}-%{sover}/
%{_libdir}/%{soname}.so
%{_libdir}/pkgconfig/dbustest-1.pc

%changelog

